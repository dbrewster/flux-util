var EventEmitter = require('events').EventEmitter, 
  Dispatcher = require('./dispatcher');

var makeStore = function (storeObj) {
  var Store = Object.create(new EventEmitter());

  for (var key in storeObj) {
    var val = storeObj[key];

    if (typeof val === 'function') {
      val = val.bind(Store);
    }

    Store[key] = val;
  }

  var handlers = storeObj.actionHandlers;

  Store.dispatchToken = Dispatcher.register(function (payload) {
    if (handlers && handlers[payload.type]) {
      Store[handlers[payload.type]](payload.data);
    }
  });

  Store.triggerChange = function () {
    this.emit('change');
  };

  return Store;
};

module.exports = makeStore;