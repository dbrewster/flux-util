var Dispatcher = require('./dispatcher');

var makeActions = function (actionsObj) {
  var Actions = {};

  for (var key in actionsObj) {
    var val = actionsObj[key];

    if (typeof val === 'function') {
      val = val.bind(Actions);
    }

    Actions[key] = val;
  }

  var handlers = actionsObj.actionHandlers;

  Actions.dispatch = function (eventName, data) {
    Dispatcher.dispatch({
      type: eventName,
      data: data
    });
  };

  return Actions;
};

module.exports = makeActions;
