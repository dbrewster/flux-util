var callbacks = {};
var pendingPayload;
var pendingCallbacks = {};
var handledCallbacks = {};
var isDispatching = false;

var Dispatcher = {

  register: function (cb) {
    var token = createToken();
    callbacks[token] = cb;
    return token;
  },

  unregister: function (token) {
    delete callbacks[token];
  },

  dispatch: function (payload) {
    if (isDispatching) throw new Error('Dispatch called while dispatching');

    isDispatching = true;
    pendingCallbacks = {};
    handledCallbacks = {};
    pendingPayload = payload;

    try {
      for (var token in callbacks) {
        if (pendingCallbacks[token]) continue;
        invokeCallback(token);
      }     
    } finally {
      isDispatching = false;
    }
  },

  waitFor: function (ids) {
    ids.forEach(function (id) {
      if (pendingCallbacks[id] && !handledCallbacks[id]) throw new Error('Dependency cycle detected');
      invokeCallback(id);
    });
  },

  isDispatching: function () {
    return isDispatching;
  }

};

var invokeCallback = function (id, payload) {
  if (!handledCallbacks[id]) {
    pendingCallbacks[id] = true;
    callbacks[id](pendingPayload);
    handledCallbacks[id] = true;
  }
};

var createToken = function () {
  return Math.random().toString(36).substring(2);
};

module.exports = Dispatcher;
