module.exports = {
  Dispatcher: require('./dispatcher'),
  makeStore: require('./make_store'),
  makeActions: require('./make_actions')
}
